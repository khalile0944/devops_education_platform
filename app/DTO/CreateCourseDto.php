<?php

namespace App\DTO;

class CreateCourseDto
{
    public string $name;
    public string $description;
    public string $image;
    public int    $teacher_id;

    public function __construct(
        string $name,
        string $description,
        string|null $image,
        int $teacher_id
    ) {
        $this->name = $name;
        $this->description = $description;
        $this->image = $image ? $image : "https://via.placeholder.com/500";
        $this->teacher_id = $teacher_id;
    }
}
