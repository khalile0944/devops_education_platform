<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'description',
        'image',
    ];

    public function course_roles()
    {
        return $this->hasMany(CourseRole::class, 'course_id', 'id');
    }

    public function users()
    {
        return $this->belongsToMany(User::class);
    }
}
