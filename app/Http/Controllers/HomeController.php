<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    //Start
    public function __construct()
    {
        $this->middleware(['auth',  'verified']);
    }

    //Home page data - users
    public function index(Request $request){
        return view('dashboard');
    }
}
