<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Repositories\UserRepository;
use App\Services\AppService;
use Illuminate\Http\Request;

class TeacherController extends Controller
{
    private $userRepository;
    public function __construct(UserRepository $userRepository)
    {
        $this->middleware(['auth', 'role:admin']);
        $this->userRepository = $userRepository;
    }

    public function add(Request $req)
    {
        $users = $this->userRepository->searchFilter()->query()->paginate(env('paginate'));

        return view('pages.teachers.add', compact('users'));
    }
    public function store($id)
    {
        AppService::changeRole('teacher', $id);
        return redirect()->route('teacher.add')->with('success', __('translation.add_success'));
    }
}
