<?php

namespace App\Http\Controllers\Admin;

use App\DTO\CreateCourseDto;
use App\Http\Controllers\Controller;
use App\Http\Requests\CourseRequestCreate;
use App\Repositories\TeacherRepository;
use App\Repositories\UserRepository;
use App\Services\admin\CourseService;
use Illuminate\Http\Request;

class CourseController extends Controller
{
    private $teacherRepository;
    public function __construct(TeacherRepository $teacherRepository)
    {
        $this->middleware(['auth', 'role:admin']);
        $this->teacherRepository = $teacherRepository;
    }

    public function add()
    {
        $teachers = $this->teacherRepository->get();
        return view('pages.courses.add', compact('teachers'));
    }

    public function create(CourseRequestCreate $req)
    {
        $image_url = null;
        $req->image != null ?
            $image_url = $req->file('image')->store('images', 'private') :
            $image_url = null;

        $createCourse = new CreateCourseDto($req->name, $req->description, $image_url, $req->teacher_id);
        CourseService::create($createCourse);
        return redirect()->route('course.add')->with('success', __('translation.add_success'));
    }

    public function edit()
    {
    }

    public function update()
    {
    }
}
