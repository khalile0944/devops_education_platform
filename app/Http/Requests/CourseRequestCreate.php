<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Spatie\Permission\Models\Role;

class CourseRequestCreate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'name'        => ['required', "min:4", "max:100"],
            'description' => ['required', "min:20", "max:255"],
            'image'       => ['image', 'max:4048'],
            'teacher_id'  => [
                'required',
                'exists:users,id',
                Rule::exists('model_has_roles', 'model_id')
                    ->where('role_id', Role::where('name', 'teacher')->first()->id),
            ],
        ];
    }
}
