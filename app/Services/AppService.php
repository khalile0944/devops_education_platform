<?php

namespace App\Services;

use App\Models\User;

class AppService
{
    public static function changeRole($role, $id): void
    {
        $user = User::findOrFail($id);
        $user->removeRole($user->roles->first()); // Remove the first assigned role
        $user->assignRole($role);
    }
}
