<?php

namespace App\Services\admin;

use App\DTO\CreateCourseDto;
use App\Models\Course;
use App\Models\CourseRole;

class CourseService
{
    public static function create(CreateCourseDto $createCourse): void
    {
        $course = Course::create([
            "name"        => $createCourse->name,
            "description" => $createCourse->description,
            "image"       => $createCourse->image,
        ]);
        //register course to teacher
        CourseRole::create([
            'user_id'   => $createCourse->teacher_id,
            'course_id' => $course->id,
            'role'      => 1, //teacher
        ]);
    }
}
