<?php

namespace App\Repositories;

use App\Models\User; // Import the model you want to interact with

class UserRepository extends Repository
{

    public function __construct(User $user)
    {
        $this->query = $user->query();
        $this->query->whereHas('roles', function ($q) {
            $q->where('name', 'user');
        });
    }


    public function searchFilter()
    {
        if (request('search')) {
            $this->query->where(function ($query) {
                $query->where('users.name', 'LIKE', '%' . request('search') . '%')
                    ->orWhere('users.email', 'LIKE', '%' . request('search') . '%')
                    ->orWhere('users.id', 'LIKE', '%' . request('search') . '%');
            });
        }
        return $this;
    }
}
