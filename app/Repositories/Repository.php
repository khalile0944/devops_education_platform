<?php

namespace App\Repositories;


class Repository
{
    protected $query;

    // to return $query
    public function query()
    {
        return $this->query;
    }
    
    // to return result of $query
    public function get()
    {
        return $this->query->get();
    }
}
