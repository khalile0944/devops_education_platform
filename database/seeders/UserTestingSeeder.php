<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

// need to run seed of role and permisssion
class UserTestingSeeder extends Seeder
{

    public function run(): void
    {
        $this->call(UserSeeder::class);
        User::create([
            "name"     => "teacher",
            "email"    => "teacher@teacher.teacher",
            "password" => Hash::make("123456789"),
        ])->assignRole('teacher');
        User::create([
            "name"     => "user",
            "email"    => "user@user.user",
            "password" => Hash::make("123456789"),
        ])->assignRole('user');
    }
}
