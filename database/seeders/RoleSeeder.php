<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function RoleCreateWebGuard(String $name){
        return Role::create(['name' => $name, 'guard_name' => 'web']);
    }
    public function run(): void
    {
        //
        $adminRole =  $this->RoleCreateWebGuard('admin');
        $teacherRole = $this->RoleCreateWebGuard('teacher');
        $user = $this->RoleCreateWebGuard('user');
        
        // Retrieve all permission names
        $permissionNames = Permission::pluck('name')->all();
        foreach ($permissionNames as $key => $value) {
            $adminRole->givePermissionTo($value);
        }

    }
}
