<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;


// need to run seed of role and permisssion
class UserSeeder extends Seeder
{

    public function run(): void
    {
        User::create([
            "name"     => "admin",
            "email"    => "admin@admin.admin",
            "password" => Hash::make("k0944940917"),
        ])->assignRole('admin');
    }
}
