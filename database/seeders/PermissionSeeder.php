<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function PermissionCreateWebGuard(String $name)
    {
        return Permission::create(['name' => $name, 'guard_name' => 'web']);
    }
    public function run(): void
    {
        //
        $this->PermissionCreateWebGuard('view_users');
        $this->PermissionCreateWebGuard('create_users');
        $this->PermissionCreateWebGuard('edit_users');
        $this->PermissionCreateWebGuard('delete_users');
        $this->PermissionCreateWebGuard('view_teachers');
        $this->PermissionCreateWebGuard('add_teachers');
        $this->PermissionCreateWebGuard('delete_teachers');
        $this->PermissionCreateWebGuard('add_courses');
        $this->PermissionCreateWebGuard('edit_courses');
    }
}
