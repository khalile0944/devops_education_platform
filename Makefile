
serve:
	php artisan serve

migrate_fresh_seeder:
	php artisan migrate:fresh --seed

migrate_fresh_test_seeder:
	php artisan migrate:fresh
	php artisan db:seed --class=TestingSeeder

test:
	php artisan test

push:
	git add -A
	git commit -m '$(m)'
	git push

