<?php

use App\Http\Controllers\Admin\CourseController;
use App\Http\Controllers\Admin\TeacherController;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Auth::routes();

Route::get('/', function () {
    return redirect('/home');
})->name('root');

Route::get('/home', [HomeController::class, 'index'])->name('dashboard');
//teacher
Route::prefix('teacher')->name('teacher.')->group(function () {
    Route::get('/show', [TeacherController::class, 'show'])->name('show');
    Route::get('/add', [TeacherController::class, 'add'])->name('add');
    Route::get('/store/{id}', [TeacherController::class, 'store'])->name('store');
});

//course
Route::prefix('courses')->name('course.')->group(function () {
    Route::get('/add', [CourseController::class, 'add'])->name('add');
    Route::post('/add', [CourseController::class, 'create'])->name('create');

    Route::get('/edit', [CourseController::class, 'edit'])->name('edit');
    Route::post('/edit', [CourseController::class, 'update'])->name('update');
});
