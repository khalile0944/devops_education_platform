<?php

return [
    //Errors
    "backtodashboard"    => "Back to Dashboard",
    "notfound"            => "Sorry, page not found",
    "Error_404"            => "Not found",

    "app_title"         => "devops_education_platform",
    "welcome_back"      => "welcome back to devops",
    "sign_in"           => "sign in",
    "email"             => "email",
    "email_entr"        => "enter email ",
    "password"          => "password",
    "password_entr"     => "enter password",
    "remember"          => "remember",
    "login"             => "login",
    "signup"            => "sign up",
    "noaccount"         => "no account ",
    "acc_register"      => "register",
    "acc_start"         => "account start",
    "acc_fname"         => "full name",
    "acc_fename"        => "enter full name",
    "acc_passconf"      => "password confirm",
    "acc_haveacc"       => "have account",
    "acc_login"         => "login",
    "login"             => "login",
    "register"          => "register",
    "Profile"           => "Profile",
    "change_password"   => "Change password",
    "Logout"            => "Logout",
    "id"                => "id",
    "search"            => "search",
    "save_changes"      => "Save Changes",
    "image"             => "Image",
    "add_success"       => "Added successfully",
    "dashboard"         => [
        "name"              => "dashboard",
        "welcome"           => "welcome",
        "short"             => "this short descript for dashboard",
        "order"             => "order",
        "revenue"           => "revenue",
    ],
    "teacher"           => [
        "manage"            => "Manage teachers",
        "add"               => "Add teachers",
        "show"              => "Show Teachers",
    ],
    "user"              => [
        "all"               => "All user",
        "name"              => "Name",
        "email"             => "Email",
    ],
    "action"            => [
        "name"              => "name",
        "addTeacher"        => "Add teacher",
    ],
    "course"            => [
        "manage"            => "Manage courses",
        "add"               => "Add courses",
        "show"              => "Show courses",
        "edit"              => "Edit courses",
        "name"              => "Name of course",
        "description"       => "Description of course",
        "image"             => "Image of course",
        "assigned"          => "Assigned course"
    ]
];
