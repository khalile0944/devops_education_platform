<?php

return [
    "customer_review_user" => "- Watslly User",
    "customer_review_title" => "Customer Reviews",
    "customer_review" => array(
        "0" => array(
            "name" => "NADIR HAMDI",
            "text" => "Thank you, Watslly team for a distinguished service that helped us develop our e-commerce thanks to their creative marketing ideas.",
        ),
        "1" => array(
            "name" => "ALADDIN",
            "text" => "I used the Watslly platform for more than two months, and customers really preferred to order via WhatsApp for speed and ease.",
        ),

    ),
];
