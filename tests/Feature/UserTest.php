<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UserTest extends TestCase
{
    use DatabaseTransactions, WithFaker;

    private function setUpResgisterUser()
    {
        $response = $this->post('/register', [
            'name' => 'Ahmad',
            'email' => 'ewr@asd.com',
            'password' => '12345',
            'password_confirmation'=> '12345',
        ]);
        return $response;
    }

    public function testViewRegister()
    {
        $response = $this->get('/register');
        $response->assertStatus(200); // Check if the register page exists
    }

    public function testViewLogin()
    {
        $response = $this->get('/login');
        $response->assertStatus(200); // Check if the login page exists

    }



    public function test_a_visitor_can_register_a_new_account()
    {
        $response = $this->setUpResgisterUser();

        $response->assertStatus(302); // Check if the registration request was successful
        $response->assertRedirect('/home'); // Assuming the redirect path after registration is '/home'
        $this->assertAuthenticated(); // Check if the user is authenticated
        $this->assertDatabaseHas('users', ['email' => 'ewr@asd.com']);
    }


    public function test_a_visitor_cannot_register_with_existing_email()
    {
        User::factory()->create([
            'email' => 'ewr@asd.com',
        ]);

        $response = $this->setUpResgisterUser();

        $response->assertSessionHasErrors(['email']); // Check if the email validation error is present
    }

    public function test_registration_requires_valid_data()
    {
        $response = $this->post('/register', [
            'name' => 'ah',
            'email' => 'ew121r@asd21.com',
            'password' => '12345',
            'password_confirmation'=> '12345',
        ]);

        $response->assertSessionHasErrors(['name']); // Check if the name validation error is present
    }

    public function test_registration_requires_valid_name_and_email()
    {
        $response = $this->post('/register', [
            'name' => 'ah',
            'email' => 'ew121r',
            'password' => '12345',
            'password_confirmation'=> '12345',
        ]);

        $response->assertSessionHasErrors(['name', 'email']); // Check if both name and email validation errors are present
    }

    public function test_a_user_can_login_with_correct_credentials()
    {
        User::factory()->create([
            'name' => 'Ahmad',
            'email' => 'ewr@asd.com',
            'password' => bcrypt('12345'),
        ]);

        $response = $this->post('/login', [
            'email' => 'ewr@asd.com',
            'password' => '12345',
        ]);

        $response->assertStatus(302); // Check if the login request was successful
        $response->assertRedirect('/home'); // Assuming the redirect path after login is '/home'
        $this->assertAuthenticated(); // Check if the user is authenticated
    }

    public function test_a_user_cannot_login_with_incorrect_credentials()
    {
        User::factory()->create([
            'name' => 'Ahmad',
            'email' => 'ewr@asd.com',
            'password' => bcrypt('12345'),
        ]);

        $response = $this->post('/login', [
            'email' => 'ewr@asd.com',
            'password' => 'wrong_password',
        ]);

        $response->assertSessionHasErrors(['email']); // Check if the email validation error is present
    }

    public function test_login_requires_valid_data()
    {
        $response = $this->post('/login', [
            'email' => 'ew121r',
            'password' => '12345',
        ]);

        $response->assertSessionHasErrors(['email']); // Check if the email validation error is present
    }



}
