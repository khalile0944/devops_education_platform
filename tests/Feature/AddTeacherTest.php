<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;


class AddTeacherTest extends TestCase
{
    use DatabaseTransactions, WithFaker;

    // Simulate an authenticated user with admin 
    public function test_authenticated_admin_can_access_page()
    {
        $admin = User::factory()->create()->assignRole('admin');
        $response = $this->actingAs($admin)->get(route('teacher.add'));
        $response->assertStatus(200);
    }

    // Simulate an authenticated user
    public function test_authenticated_user_cannot_access_page()
    {
        $user = User::factory()->create()->assignRole('user');
        $response = $this->actingAs($user)->get(route('teacher.add'));
        $response->assertStatus(403); // USER DOES NOT HAVE THE RIGHT ROLES
    }

    // Simulate an authenticated user
    public function test_authenticated_teacher_cannot_access_page()
    {
        $teacher = User::factory()->create()->assignRole('teacher');
        $response = $this->actingAs($teacher)->get(route('teacher.add'));
        $response->assertStatus(403); // USER DOES NOT HAVE THE RIGHT ROLES
    }

    // Simulate an authenticated user without a valid token
    public function test_user_with_invalid_token_redirected_to_login_page()
    {
        $response = $this->withCookie('token', 'invalid_token')->get(route('teacher.add'));
        $response->assertRedirect(route('login'));
    }

    public function test_list_non_teacher_users()
    {
        // Simulate users where some are teachers and some are not
        $teacher = User::factory()->create()->assignRole('teacher');
        $nonTeacher = User::factory()->create()->assignRole('user');

        // Simulate an authenticated user with admin privileges
        $admin = User::factory()->create()->assignRole('admin');
        $response = $this->actingAs($admin)->get(route('teacher.add'));

        $response->assertSee($nonTeacher->name);
        $response->assertDontSee($teacher->name);
    }

    
    public function test_store_method_changes_user_role_and_redirects()
    {
        $user = User::factory()->create()->assignRole('user');
        $admin = User::factory()->create()->assignRole('admin');

        $response = $this->actingAs($admin)->get(route('teacher.store', ['id' => $user->id]));
        $response->assertRedirect(route('teacher.add'));
        $response->assertSessionHas('success', __('translation.add_success'));
    }
    
}
