@extends('layouts.master')

@section('title')
    @lang('translation.dashboard.name')
@endsection

@section('content')
    @component('components.breadcrumb')
        @slot('li_1')
            @lang('translation.dashboard.name')
        @endslot
        @slot('title')
            @lang('translation.dashboard.name')
        @endslot
    @endcomponent

    <div class="row">
        <div class="col-xl-4">
            <div class="card overflow-hidden">
                <div class="bg-primary bg-soft">
                    <div class="row">
                        <div class="col-7">
                            <div class="text-primary p-3">
                                <h5 class="text-primary">@lang('translation.dashboard.welcome')</h5>
                                <p>@lang('translation.dashboard.short') </p>
                            </div>
                        </div>
                        <div class="col-5 align-self-end">
                            {{-- <img src="{{ URL::asset('/build/images/profile-img.png') }}" alt="" class="img-fluid"> --}}
                        </div>
                    </div>
                </div>
                <div class="card-body pt-0">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="avatar-md profile-user-wid mb-4">
                                <img src="{{ isset(Auth::user()->avatar) ? asset(Auth::user()->avatar) : asset('/build/images/users/1689045779.png') }}"
                                    alt="" class="img-thumbnail rounded-circle">
                            </div>
                            <h5 class="font-size-15 text-truncate">
                                {{ Str::ucfirst(Auth::user()->first_name . ' ' . Auth::user()->last_name) }}</h5>
                            <!--  <p class="text-muted mb-0 text-truncate">UI/UX Designer</p>-->
                        </div>

                        <div class="col-sm-8">
                            <div class="pt-4">
                                <div class="row">
                                    <div class="col-6">
                                        <h5 class="font-size-15">0</h5>
                                        <p class="text-muted mb-0">@lang('translation.dashboard.order')</p>
                                    </div>
                                    <div class="col-6">
                                        <h5 class="font-size-15">0</h5>
                                        <p class="text-muted mb-0">@lang('translation.dashboard.revenue')</p>
                                    </div>
                                </div>
                                <div class="mt-4">
                                    <a href=""
                                        class="btn btn-primary waves-effect waves-light btn-sm float-end">@lang('translation.Profile')
                                        <i class="mdi mdi-arrow-right ms-1"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        {{-- @foreach ($semesters as $semester)
            <div class="col-xl-3 col-md-6">
                <div class="card">
                    <div class="card-body">
                        <h5 class="fs-17 mb-2">{{ $semester->name }}</h5>
                        <h6 class="fs-17 mb-2">{{ $semester->program->name }} ({{ $semester->programYear->year }})</h4>
                        <p class="fs-17 mb-2">{{ $semester->short_desc }}</p>
                        <ul class="d-flex flex-column list-inline mb-0" style="font-size: 14px;">
                            <li class="list-inline-item">
                                <p class="text-muted fs-16 mb-0"><i class="mdi mdi-credit-card"></i>
                                    <b>@lang('translation.price'):</b> {{ $semester->price }} @lang('translation.sar')
                                </p>
                            </li>
                            @if ($semester->first_payment != $semester->price)
                            <li class="list-inline-item">
                                <p class="text-muted fs-16 mb-0"><i class="mdi mdi-credit-card-check"></i>
                                    <b>@lang('translation.first_payment'):</b> {{ $semester->first_payment }} @lang('translation.sar')
                                </p>
                            </li>
                            @endif
                            <li class="list-inline-item">
                                <p class="text-muted fs-16 mb-1"><i class="mdi mdi-calendar-start"></i>
                                    <b>@lang('translation.date_start'):</b> {{ $semester->date_start }}
                                </p>
                            </li>
                            <li class="list-inline-item">
                                <p class="text-muted fs-16 mb-0"><i class="mdi mdi-calendar-end"></i>
                                    <b>@lang('translation.date_end'):</b> {{ $semester->date_end }}
                                </p>
                            </li>
                        </ul>
                        <div class="mt-3 hstack gap-2">
                            <span class="badge rounded-1 badge-soft-warning"><i class="mdi mdi-account"></i>
                                {{ $semester->capacity }}</span>
                        </div>
                        <div class="mt-4 hstack gap-2">
                            <a href="{{ route('courses', $semester->id) }}" class="btn btn-secondary w-100">
                                @lang('translation.program.enter')
                            </a>
                            <a  href="#" data-bs-toggle="modal" data-bs-target="#myModal{{ $semester->id }}" class="btn btn-warning w-100">@lang('translation.courses_chk')</a>
                            <!-- sample modal content -->
                            <div id="myModal{{ $semester->id }}" class="modal fade" tabindex="-1" aria-labelledby="myModalLabel" style="display: none;" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="myModalLabel">@lang('translation.courses_chk')</h5>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                        </div>
                                        <div class="modal-body">
                                            <h5>@lang('translation.courses')</h5>
                                            <p>
                                            @foreach ( $semester->courses as $course )
                                                <strong> @lang('translation.course_name') </strong> : {{ $course->name }}
                                                <br>
                                                {{ $course->short_desc }}
                                                <br>
                                                <strong> @lang('translation.trainer') </strong> : {{ $course->user->first_name }} {{ $course->user->last_name }}
                                                <br>
                                                <strong> @lang('translation.full_point') </strong> : {{ $course->full_point }}
                                                <strong> @lang('translation.fail_point') </strong> : {{ $course->fail_point }}
                                                <strong> @lang('translation.permanency_require') </strong> : {{ $course->permanency_require ? trans('translation.yes') : trans('translation.no')  }}
                                                <br><br>
                                            @endforeach
                                            </p>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary waves-effect" data-bs-dismiss="modal">@lang('translation.close')</button>
                                        </div>
                                    </div><!-- /.modal-content -->
                                </div><!-- /.modal-dialog -->
                            </div><!-- /.modal -->
                        </div>
                    </div>
                </div>
            </div>
        @endforeach --}}
    </div>
    <!-- end row -->

@endsection
@section('script')
    <!-- apexcharts -->
    <script src="{{ URL::asset('/build/libs/apexcharts/apexcharts.min.js') }}"></script>

    <!-- dashboard init -->
    <script src="{{ URL::asset('build/js/pages/dashboard.init.js') }}"></script>
@endsection
