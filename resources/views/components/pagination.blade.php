@if ($paginator->hasPages())
    <ul class="pagination pagination-rounded justify-content-end mb-2">
        {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())
            <li class="page-item disabled">
                <a class="page-link" href="javascript: void(0);" aria-label="Previous">
                    <i class="mdi mdi-chevron-left"></i>
                </a>
            </li>
        @else
            <li class="page-item">
                <a class="page-link" href="{{ $paginator->previousPageUrl() }}" aria-label="Previous">
                    <i class="mdi mdi-chevron-left"></i>
                </a>
            </li>
        @endif


        @for($i = 1; $i < $paginator->lastPage()+1; $i++)
            <li class="page-item">
                <a class="page-link {{ $paginator->currentPage() == $i ? 'active' : ''}}" href="{{ $paginator->currentPage() == $i ? 'javascript: void(0);' : $paginator->url($i) }}">{{ $i }}</a>
            </li>
        @endfor


        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
            <li class="page-item">
                <a class="page-link" href="{{ $paginator->nextPageUrl() }}" aria-label="Next">
                    <i class="mdi mdi-chevron-right"></i>
                </a>
            </li>
        @else
            <li class="page-item disabled">
                <a class="page-link" href="javascript: void(0);" aria-label="Previous">
                    <i class="mdi mdi-chevron-right"></i>
                </a>
            </li>
        @endif
    </ul>
@endif
