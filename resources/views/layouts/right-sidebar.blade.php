<!-- Right Sidebar -->
<div class="right-bar">
    <div class="vertical-menu">
        <div data-simplebar class="h-100">
            <!--- Sidemenu -->
            <div id="sidebar-menu">
                <!-- Left Menu Start -->
                <ul class="metismenu list-unstyled" id="side-menu">
                    <!--<li class="menu-title" key="t-menu">@lang('translation.Menu')</li>-->

                    <li>
                        <a href="dashboard" class="" key="t-default">
                            <i class="bx bx-home-circle"></i>
                            <span key="t-dashboards">@lang('translation.Dashboards')</span>
                        </a>
                    </li>

                    <li>
                        @if (auth()->user()->hasRole('administrator'))
                        <a href="{{ route('products.admin') }}" class="" key="t-default">
                            <i class="bx bx-store"></i>
                            <span key="t-ecommerce">@lang('translation.products')</span>
                        </a>
                        @else
                        <a href="{{ route('products.user', auth()->user()->id) }}" class="" key="t-default">
                            <i class="bx bx-store"></i>
                            <span key="t-ecommerce">@lang('translation.products')</span>
                        </a>
                        @endif
                    </li>

                    <!--<li class="menu-title" key="t-apps">@lang('translation.Apps')</li>-->
                </ul>
            </div>
            <!-- Sidebar -->
        </div>
    </div>
</div>
<!-- /Right-bar -->

<!-- Right bar overlay-->
<div class="rightbar-overlay"></div>
