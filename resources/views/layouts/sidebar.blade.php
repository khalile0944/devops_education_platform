<!-- ========== Left Sidebar Start ========== -->
<div class="vertical-menu">
    <div data-simplebar class="h-100">
        <!--- Sidemenu -->
        <div id="sidebar-menu">
            <!-- Left Menu Start -->
            <ul class="metismenu list-unstyled" id="side-menu">
                <!--<li class="menu-title" key="t-menu">@lang('translation.Menu')</li>-->

                <li>
                    <a href="/dashboard" class="waves-effect" key="t-default">
                        <i class="bx bx-home-circle"></i>
                        <span key="t-dashboards">@lang('translation.dashboard.name')</span>
                    </a>

                </li>
                @can('user_info')
                    <li class="">
                        <a href="javascript: void(0);" class="has-arrow waves-effect" aria-expanded="false">
                            <i class="bx bx-user-circle"></i>
                            <span key="t-blog">@lang('translation.users')</span>
                        </a>
                        <ul class="sub-menu mm-collapse" aria-expanded="false" style="height: 0px;">

                            <li><a href="{{ route('users.index') }}" key="t-users-add">
                                    @lang('translation.managersusers')</a>
                            </li>
                            <li><a href="{{ route('users.trainee') }}" key="t-users-add">
                                    @lang('translation.alltrainee')</a>
                            </li>
                            {{-- <li><a href="{{ route('users.create') }}" key="t-users-add">
                                    @lang('translation.user_create')</a>
                            </li> --}}

                        </ul>
                    </li>
                @endcan

                @can('payments')
                    <li class="">
                        <a href="javascript: void(0);" class="has-arrow waves-effect" aria-expanded="false">
                            <i class="bx bx-dollar"></i>
                            <span key="t-blog">@lang('translation.Bills')</span>
                        </a>
                        <ul class="sub-menu mm-collapse" aria-expanded="false" style="height: 0px;">
                            <li><a href="{{ route('bills') }}" key="t-users-add">
                                    @lang('translation.AllBills')</a>
                            </li>
                        </ul>
                    </li>
                @endcan



                @if (Auth::check() && Auth::user()->hasRole('admin'))
                    <li class="">
                        <a href="javascript: void(0);" class="has-arrow waves-effect" aria-expanded="false">
                            <i class="bx bx-book-open"></i>
                            <span key="t-blog">@lang('translation.teacher.manage')</span>
                        </a>

                        <ul class="sub-menu mm-collapse" aria-expanded="false" style="height: 0px;">
                            <li><a href="{{ route('teacher.add') }}" key="t-users-add">
                                    @lang('translation.teacher.add')</a>
                            </li>
                            <li><a href="" key="t-users-add">
                                    @lang('translation.teacher.show')</a>
                            </li>

                        </ul>
                    </li>
                @endif

                @if (Auth::check() && Auth::user()->hasRole('admin'))
                    <li class="">
                        <a href="javascript: void(0);" class="has-arrow waves-effect" aria-expanded="false">
                            <i class="bx bx-book-open"></i>
                            <span key="t-blog">@lang('translation.course.manage')</span>
                        </a>

                        <ul class="sub-menu mm-collapse" aria-expanded="false" style="height: 0px;">
                            <li><a href="{{ route('course.add') }}" key="t-users-add">
                                    @lang('translation.course.add')</a>
                            </li>
                            <li><a href="{{ route('course.edit') }}" key="t-users-add">
                                    @lang('translation.course.edit')</a>
                            </li>

                        </ul>
                    </li>
                @endif
            </ul>
        </div>
        <!-- Sidebar -->
    </div>
</div>
<!-- Left Sidebar End -->
