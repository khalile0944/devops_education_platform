<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" style=" direction:@lang('interface.dir') !important">

    <head>
        <meta charset="utf-8" />
        <title> @yield('title') @lang('translation.app_title')</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta content="@lang('translation.app_description')" name="description" />
        <meta content="@lang('translation.app_author')" name="author" />
        <!-- App favicon -->
        <link rel="shortcut icon" href="{{ URL::asset('build/images/favicon.ico')}}">
        @include('layouts.head-css')
  </head>
    <body dir="@lang('interface.dir')">
    @yield('body')

    @yield('content')

    @include('layouts.vendor-scripts')
    </body>
</html>
