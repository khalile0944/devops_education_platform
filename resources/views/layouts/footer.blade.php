<footer class="footer">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-6">
                <p class="mb-0">© <script>
                    document.write(new Date().getFullYear())
                </script> @lang('translation.copyright2')</p>
            </div>
            <div class="col-sm-6">
                <div class="text-sm-end d-none d-sm-block">
                    <p class="mb-0"> @lang('translation.copyright3') <i class="mdi mdi-heart text-danger"></i> @lang('translation.by')
                    <a target="_blank" href="https://yashamturk.com/">@lang('translation.yasham')</a></p>
                </div>
            </div>
        </div>
    </div>
</footer>