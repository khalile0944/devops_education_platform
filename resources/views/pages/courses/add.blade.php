@extends('layouts.master')

@section('title')
    @lang('translation.course.add')
@endsection

@section('css')
    <!-- select2 css -->
    <link href="{{ URL::asset('/build/libs/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />

    <!-- dropzone css -->
    <link href="{{ URL::asset('build/libs/dropzone/min/dropzone.min.css') }}" rel="stylesheet" type="text/css" />

    <!-- Phone flag block -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.3/css/intlTelInput.min.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.3/js/intlTelInput.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/11.0.9/js/utils.js"></script>
    <!-- Phone flag block end -->
@endsection

@section('content')
    @component('components.alert')
    @endcomponent

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="col-12 d-flex flex-wrap">
                        <div class="">
                            <h4 class="">@lang('translation.course.add')</h4>
                            <p class="card-title-desc">@lang('translation.course.add')</p>
                        </div>
                    </div>

                    <form method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col-sm-9">
                                <div class="mb-3 col-sm-6">
                                    <label for="name">@lang('translation.course.name')*</label>
                                    <input id="name" name="name" type="text"
                                        class="form-control @error('name') is-invalid @enderror"
                                        placeholder="@lang('translation.course.name')" value="{{ old('name') }}" required>
                                    @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="mb-3 col-sm-6">
                                    <label for="teacher_id" class="form-label">@lang('translation.course.assigned')*</label>
                                    <select class="form-control @error('teacher_id') is-invalid @enderror" id="teacher_id"
                                        name="teacher_id" required>
                                        <option value="">@lang('translation.course.assigned')</option>
                                        @foreach ($teachers as $teacher)
                                            <option value="{{ $teacher->id }}"
                                                {{ old('teacher_id') == $teacher->id ? 'selected' : '' }}>
                                                {{ $teacher->name }}</option>
                                        @endforeach
                                    </select>
                                    @error('teacher_id')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="mb-3">
                                    <label for="description">@lang('translation.course.description')*</label>
                                    <input id="description" name="description" type="text"
                                        class="form-control @error('description') is-invalid @enderror"
                                        placeholder="@lang('translation.course.description')" value="{{ old('description') }}" required>
                                    @error('description')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>

                                <div class="d-flex flex-wrap gap-2">
                                    <button type="submit"
                                        class="btn btn-primary waves-effect waves-light">@lang('translation.save_changes')</button>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <h4 class="sub_address mb-4 font-size-16">@lang('translation.image')</h4>
                                <div class="mb-12 row">
                                    <div class="mb-3">
                                        <label for="image">&#160; @lang('translation.image')*</label>
                                        <input id="image" name="image" type="file" accept="image/*"
                                            class="form-control @error('image') is-invalid @enderror">
                                        @error('image')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
    <!-- end row -->
@endsection
@section('script')
    <!-- phone flasg block -->
    <script src="{{ URL::asset('/js/custom_tel.js') }}"></script>
@endsection
