@extends('layouts.master')

@section('title')
    @lang('translation.users')
@endsection
@section('css')
    <!-- bootstrap-datepicker css -->
    <link href="{{ URL::asset('build/libs/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}" rel="stylesheet"
        type="text/css">

    <!-- DataTables -->
    <link href="{{ URL::asset('build/libs/datatables.net-bs4/css/dataTables.bootstrap4.min.css') }}" rel="stylesheet"
        type="text/css" />

    <!-- Responsive datatable examples -->
    <link href="{{ URL::asset('build/libs/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css') }}"
        rel="stylesheet" type="text/css" />
@endsection
@section('content')
    @component('components.alert')
    @endcomponent

    <div class="row">

        <h1> @lang('translation.user.all') </h1>
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="row mb-2">
                        <div class="col-sm-12">
                            <form method="GET" action="" class="d-flex" style="justify-content: space-between;">
                                <div class="mb-2 search-box col-sm-2">
                                    <div class="position-relative">
                                        <input type="text" class="form-control" id="search" name="search"
                                            placeholder="@lang('translation.search')" value="{{ Request::get('search') }}">
                                        <i class="bx bx-search-alt search-icon"></i>
                                    </div>
                                </div>

                            </form>
                        </div>
                        <!-- end col-->
                    </div>
                    <div class="table-responsive">
                        <table class="table align-middle table-nowrap dt-responsive nowrap w-100" id="productsList-table">
                            <thead class="table-light">
                                <tr>
                                    <th class="col-1">@lang('translation.id')</th>
                                    <th class="col-2">@lang('translation.user.name')</th>
                                    <th class="col-2">@lang('translation.user.email')</th>
                                    <th class="col-3">@lang('translation.action.name')</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($users as $user)
                                    <tr>
                                        <td>{{ $user->id }}</td>
                                        <td>{{ $user->name }}</td>
                                        <td>{{ $user->email }}</td>
                                        <td>
                                            <div class="d-flex gap-3">
                                                <a href="{{ route('teacher.store', $user->id) }}"
                                                    class="btn btn-light btn-rounded waves-effect">
                                                    <i class="mdi mdi-account-plus font-size-14"></i>
                                                    @lang('translation.action.addTeacher')
                                                </a>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <!-- end table -->
                    </div>
                    {!! $users->links('components.pagination') !!}
                    <!-- end table responsive -->
                </div>
                <!-- end card body -->
            </div>
            <!-- end card -->
        </div>
        <!-- end col -->
    </div>
    <!-- end row -->
@endsection

@section('script')
    <!-- bootstrap-datepicker js -->
    <script src="{{ URL::asset('build/libs/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>

    <!-- Required datatable js -->
    <script src="{{ URL::asset('build/libs/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ URL::asset('build/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js') }}"></script>

    <!-- Responsive examples -->
    <script src="{{ URL::asset('build/libs/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ URL::asset('build/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js') }}"></script>

    <!-- ecommerce-customer-list init
                                                                                                                                                                                                                        <script src="{{ URL::asset('build/js/pages/ecommerce-customer-list.init.js') }}"></script>-->
@endsection
