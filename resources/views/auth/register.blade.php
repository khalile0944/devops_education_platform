@extends('layouts.master-without-nav')

@section('title')
    @lang('translation.register')
@endsection

@section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/3.5.0/css/flag-icon.min.css"
        integrity="sha512-..." crossorigin="anonymous" />
    <!-- owl.carousel css -->
    <link rel="stylesheet" href="{{ URL::asset('/build/libs/owl.carousel/assets/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('/build/libs/owl.carousel/assets/owl.theme.default.min.css') }}">

    <!-- Phone flag block -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.3/css/intlTelInput.min.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.3/js/intlTelInput.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/11.0.9/js/utils.js"></script>
    <!-- Phone flag block end -->
@endsection

@section('body')

    <body class="auth-body-bg" style="text-align: @lang('interface.align') !important">
    @endsection

    @section('content')
        <div>
            <div class="container-fluid p-0">
                <div class="row g-0">
                    <div class="col-xl-12">
                        <div class="auth-full-page-content p-md-5 p-4">
                            <div class="w-100">

                                <div class="d-flex flex-column h-100">

                                    <div class="my-auto">

                                        <div>
                                            <center>
                                                <h3 class="text-primary">@lang('translation.acc_register')</h3>
                                                <h4 class="text-muted">@lang('translation.acc_start')</h4>
                                            </center>
                                        </div>

                                        <div class="mt-4">
                                            <form method="POST" class="form-horizontal" action="{{ route('register') }}"
                                                enctype="multipart/form-data">
                                                @csrf

                                                <div class="mb-3">
                                                    <label for="name" class="form-label">@lang('translation.acc_fname')</label>
                                                    <input type="text"
                                                        class="form-control @error('name') is-invalid @enderror"
                                                        value="{{ old('name') }}" id="name" name="name"
                                                        autofocus required placeholder="@lang('translation.acc_fename')">
                                                    @error('name')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>

                                                <div class="mb-3">
                                                    <label for="useremail" class="form-label">@lang('translation.email')</label>
                                                    <input type="email1"
                                                        class="form-control @error('email') is-invalid @enderror"
                                                        id="useremail" value="{{ old('email') }}" name="email"
                                                        placeholder="@lang('translation.email_entr')" autofocus required>
                                                    @error('email')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>

                                                <div class="mb-3">
                                                    <label for="userpassword"
                                                        class="form-label">@lang('translation.password')</label>
                                                    <input type="password"
                                                        class="form-control @error('password') is-invalid @enderror"
                                                        id="userpassword" name="password"
                                                        placeholder="@lang('translation.password_entr')" autofocus required>
                                                    @error('password')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>

                                                <div class="mb-3">
                                                    <label for="confirmpassword"
                                                        class="form-label">@lang('translation.acc_passconf')</label>
                                                    <input type="password"
                                                        class="form-control @error('password_confirmation') is-invalid @enderror"
                                                        id="confirmpassword" name="password_confirmation"
                                                        placeholder="@lang('translation.acc_passconf')" autofocus required>
                                                    @error('password_confirmation')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>

                                                <div class="mt-4 d-grid">
                                                    <button class="btn btn-primary waves-effect waves-light"
                                                        type="submit">@lang('translation.acc_register')</button>
                                                </div>

                                            </form>

                                            <div class="mt-3 text-center">
                                                <p>@lang('translation.acc_haveacc') <a href="{{ url('login') }}"
                                                        class="fw-medium text-primary"> @lang('translation.acc_login')</a> </p>
                                            </div>

                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                    <!-- end col -->
                </div>
                <!-- end row -->
            </div>
            <!-- end container-fluid -->
        </div>
    @endsection
    @section('script')
        <!-- owl.carousel js -->
        <script src="{{ URL::asset('/build/libs/owl.carousel/owl.carousel.min.js') }}"></script>
        <!-- auth-2-carousel init -->
        <script src="{{ URL::asset('/build/js/pages/auth-2-carousel.init.js') }}"></script>
        <!-- phone flasg block -->
        <!--<script src="{{ URL::asset('/js/custom_tel.js') }}"></script>-->
    @endsection
