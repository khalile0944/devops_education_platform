
@extends('layouts.master-without-nav')

@section('title')
    {{ __('translation.verify_email') }}
@endsection

@section('body')
@component('components.alert')
@endcomponent
    <body>
    @endsection
    @section('content')
        <div class="account-pages my-5 pt-sm-5">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="text-center mb-5 text-muted">
                            <a
                                href="/"
                                class="d-block auth-logo"
                            >
                                <img
                                    src="{{ URL::asset('/assets/images/logo.png') }}"
                                    alt=""
                                    height="80"
                                    class="auth-logo-light mx-auto"
                                >
                            </a>
                        </div>
                    </div>
                </div>
                <!-- end row -->
                <div class="row justify-content-center">
                    <div class="col-md-8 col-lg-6 col-xl-5">
                        <div class="card">
                            <div class="card-body">
                                <div class="p-2">
                                    <div class="text-center">
                                        @if (session('resent'))
                                            <div
                                                class="alert alert-success"
                                                role="alert"
                                            >
                                                @lang('auth.reverify_email')
                                            </div>
                                        @endif
                                        <div class="avatar-md mx-auto">
                                            <div class="avatar-title rounded-circle bg-light">
                                                <i class="bx bxs-envelope h1 mb-0 text-primary"></i>
                                            </div>
                                        </div>
                                        <div class="p-2 mt-4 ">
                                            <h3>{{ __('translation.verify_your_email') }}</h3>
                                            <div>
                                                <p> {{ __('translation.we_send_msg') }}</p>
                                                <p class="fw-semibold d-block"><strong>{{ auth()->user()->email }}</strong></p>
                                                <p>{{ __('translation.please_check_it') }}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="mt-5 text-center">
                            <p class="d-inline">
                            {{ __('translation.didnt_receive_an_email') }}
                            <form
                                class="d-inline"
                                method="post"
                                action="{{ route('verification.resend') }}"
                            >
                                    @csrf
                                <button
                                    type="submit"
                                    class="btn btn-primary"
                                    id="resend"
                                >{{ __('translation.resend') }}</button>
                                </form>
                            </p>
                            <p class="mb-0">© <script>
                                document.write(new Date().getFullYear())
                            </script> @lang('translation.copyright') <i class="mdi mdi-heart text-danger"></i> @lang('translation.by')
                            <a target="_blank" href="https://yashamturk.com/">@lang('translation.yasham')</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
