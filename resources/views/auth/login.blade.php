@extends('layouts.master-without-nav')

@section('title')
    @lang('translation.login')
@endsection

@section('css')
    <!-- owl.carousel css -->
    <link rel="stylesheet" href="{{ URL::asset('/build/libs/owl.carousel/assets/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('/build/libs/owl.carousel/assets/owl.theme.default.min.css') }}">
@endsection

@section('body')

    <body class="auth-body-bg" style="text-align: @lang('interface.align') !important">
    @endsection

    @section('content')
        <div>
            <div class="container-fluid p-0">
                <div class="row g-0">



                    <div class="col-xl-12">
                        <div class="auth-full-page-content p-md-5 p-4">
                            <div class="w-100">
<div class="d-flex flex-column h-100">

                                    <div class="my-auto">

                                        <div>
                                            <center>
                                                <h3 class="text-primary">@lang('translation.welcome_back')</h3>
                                                <h4 class="text-muted">@lang('translation.sign_in')</h4>
                                            </center>
                                        </div>

                                        <div class="mt-4">
                                            @component('components.alert')
                                            @endcomponent
                                            <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                                                @csrf
                                                <div class="mb-3">
                                                    <label for="email" class="form-label">@lang('translation.email')</label>
                                                    <input name="email" type="email1"
                                                        class="form-control @error('email') is-invalid @enderror"
                                                        id="email"
                                                        placeholder="@lang('translation.email_entr')"  autocomplete="email" autofocus>
                                                    @error('email')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>

                                                <div class="mb-3">
                                                    <label class="form-label">@lang('translation.password')</label>
                                                    <div
                                                        class="input-group auth-pass-inputgroup @error('password') is-invalid @enderror">
                                                        <input type="password" name="password"
                                                            class="form-control  @error('password') is-invalid @enderror"
                                                            id="userpassword"
                                                            placeholder="@lang('translation.password_entr')" aria-label="Password"
                                                            aria-describedby="password-addon">
                                                        <button class="btn btn-light " type="button"
                                                            id="password-addon"><i
                                                                class="mdi mdi-eye-outline"></i></button>
                                                        @error('password')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror

                                                    </div>
                                                </div>

                                                <div class="form-check" style=" text-align: @lang('interface.align')  !important; ">
                                                    <input class="form-check-input" type="checkbox" id="remember" style="margin:3px !important; float: @lang('interface.align')"
                                                        {{ old('remember') ? 'checked' : '' }} >
                                                    <label class="form-check-label" for="remember">
                                                        @lang('translation.remember')
                                                    </label>
                                                </div>

                                                <div class="mt-3 d-grid">
                                                    <button class="btn btn-primary waves-effect waves-light"
                                                        type="submit">@lang('translation.login')</button>
                                                </div>
                                                @if(session()->has('ban'))
                                                    <div class="mt-3 d-grid" style="color: red">
                                                        <p>@lang('translation.ban_response')</p>
                                                    </div>
                                                @endif


                                                <!--
                                                    <div class="mt-4 text-center">
                                                        <h5 class="font-size-14 mb-3">Sign in with</h5>

                                                        <ul class="list-inline">
                                                            <li class="list-inline-item">
                                                                <a href="#" class="social-list-item bg-primary text-white border-primary">
                                                                    <i class="mdi mdi-facebook"></i>
                                                                </a>
                                                            </li>
                                                            <li class="list-inline-item">
                                                                <a href="#" class="social-list-item bg-info text-white border-info">
                                                                    <i class="mdi mdi-twitter"></i>
                                                                </a>
                                                            </li>
                                                            <li class="list-inline-item">
                                                                <a href="#" class="social-list-item bg-danger text-white border-danger">
                                                                    <i class="mdi mdi-google"></i>
                                                q                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                -->
                                            </form>
                                            {{-- <a class="btn btn-primary mt-1 w-100 fbbtn" href="{{ url('auth/facebook') }}">@lang('translation.loginfb')</a> --}}
                                            <div class="mt-5 text-center">

                                                <h4>@lang('translation.noaccount')<a href="{{ url('register') }}"
                                                        class="fw-medium text-primary">@lang('translation.signup')</a>
                                                </h4>
                                            </div>

                                        </div>
                                    </div>

                                </div>


                            </div>
                        </div>
                    </div>
                    <!-- end col -->
                </div>
                <!-- end row -->
            </div>
            <!-- end container-fluid -->
        </div>
    @endsection
    @section('script')
        <!-- owl.carousel js -->
        <script src="{{ URL::asset('/build/libs/owl.carousel/owl.carousel.min.js') }}"></script>
        <!-- auth-2-carousel init -->
        <script src="{{ URL::asset('/build/js/pages/auth-2-carousel.init.js') }}"></script>
    @endsection
